#!/bin/bash

export OPENSHIFT_INSTALL_RELEASE_IMAGE_OVERRIDE=quay.io/openshift-release-dev/ocp-release:4.5.8-x86_64

OPENSHIFT_INSTALL=/usr/local/bin/openshift-install
OC=/usr/local/bin/oc
YQ=/usr/local/bin/yq
INSTALL_DIR=${PWD}
DOMAIN=apps.zews-home.org

function apply_bootstrap_etcd_hack() {
        # This is needed for now due to etcd changes in 4.4:
        # https://github.com/openshift/cluster-etcd-operator/pull/279
        while ! ${OC} get etcds cluster >/dev/null 2>&1; do
            sleep 3
        done
        echo "API server is up, applying etcd hack"
        ${OC} patch etcd cluster -p='{"spec": {"unsupportedConfigOverrides": {"useUnsupportedUnsafeNonHANonProductionUnstableEtcd": true}}}' --type=merge
}

${OPENSHIFT_INSTALL} --dir ${INSTALL_DIR} create manifests

# Add custom domain to cluster-ingress
${YQ} write --inplace ${INSTALL_DIR}/manifests/cluster-ingress-02-config.yml spec[domain] ${DOMAIN}
# Add master memory to 12 GB and 6 cpus 
# This is only valid for openshift 4.3 onwards
${YQ} write --inplace ${INSTALL_DIR}/openshift/99_openshift-cluster-api_master-machines-0.yaml spec.providerSpec.value[domainMemory] 14336
${YQ} write --inplace ${INSTALL_DIR}/openshift/99_openshift-cluster-api_master-machines-0.yaml spec.providerSpec.value[domainVcpu] 6
# Add master disk size to 31 GB
# This is only valid for openshift 4.5 onwards
${YQ} write --inplace ${INSTALL_DIR}/openshift/99_openshift-cluster-api_master-machines-0.yaml spec.providerSpec.value.volume[volumeSize] 33285996544
${YQ} write --inplace ${INSTALL_DIR}/openshift/99_openshift-cluster-api_worker-machineset-0.yaml spec.providerSpec.value.volume[volumeSize] 33285996544

export KUBECONFIG=${PWD}/auth/kubeconfig

apply_bootstrap_etcd_hack &

${OPENSHIFT_INSTALL} --dir ${INSTALL_DIR} create cluster 