#!/bin/bash

#
# Assumes your already have an install-config.yaml in your PWD
# If not run 'openshift-install create install-config' to create one
#
# Go to https://amd64.ocp.releases.ci.openshift.org/ and select a relase to get your OPENSHIFT_INSTALL_RELEASE_IMAGE_OVERRIDE
#
#


INSTALL_DIR=./ocp
export OPENSHIFT_INSTALL_RELEASE_IMAGE_OVERRIDE=quay.io/openshift-release-dev/ocp-release:4.9.0-rc.8-x86_64

if [ ! -d $INSTALL_DIR ]; then
  echo "Creating install directory"
  mkdir $INSTALL_DIR
else
  echo "Install directory already exists"
  exit
fi

if [ -f install-config.yaml ]; then
  echo "copying install-config"
  cp install-config.yaml ./ocp/
else
  echo "missing install-config"
  exit
fi

echo "creating manifests"
openshift-install --dir $INSTALL_DIR create manifests

# custome values for disk, cpu and memory of nodes
echo "modifying manifests"
yq write --inplace $INSTALL_DIR/manifests/cluster-ingress-02-config.yml spec.domain apps.ocp.zews-home.org
yq write --inplace $INSTALL_DIR/openshift/99_openshift-cluster-api_worker-machineset-0.yaml spec.template.spec.providerSpec.value.volume.volumeSize 34359738368
yq write --inplace $INSTALL_DIR/openshift/99_openshift-cluster-api_worker-machineset-0.yaml spec.template.spec.providerSpec.value.domainVcpu 4
yq write --inplace $INSTALL_DIR/openshift/99_openshift-cluster-api_worker-machineset-0.yaml spec.template.spec.providerSpec.value.domainMemory 4096
yq write --inplace $INSTALL_DIR/openshift/99_openshift-cluster-api_master-machines-0.yaml spec.providerSpec.value.volume.volumeSize 34359738368
yq write --inplace $INSTALL_DIR/openshift/99_openshift-cluster-api_master-machines-1.yaml spec.providerSpec.value.volume.volumeSize 34359738368
yq write --inplace $INSTALL_DIR/openshift/99_openshift-cluster-api_master-machines-2.yaml spec.providerSpec.value.volume.volumeSize 34359738368

echo "running install"
openshift-install --dir $INSTALL_DIR create cluster
